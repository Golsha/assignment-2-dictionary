section .bss
buffer: resb 20
section .data
newline: db 10
codes: dq '0123456789'
section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0 
    .check:
    cmp byte[rdi+rax],0
    je .end
    inc rax
    jmp .check
    .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rax, 1 
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    ;here i can push the rdi value on stack 
    ;so i will have the address that i can put in rsi
    push rdi
    mov rdi, rsp
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1
    mov rdx, 1
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    mov rsi, newline
    syscall
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    mov rax, rdi
    xor rdx, rdx ; the result of division
    xor rdi, rdi ; divider
    xor rcx, rcx ; counter
    ;mov rax,rdi
;    mov rax, 0x3214 ; what we divide
;    push rbx
;    mov rbx, 0x23 ; divider
;    div rbx
;    pop rbx
    .loop1: mov rdi, 0xA
    div rdi
    push rdx
    xor rdx, rdx
    inc rbx
    test rax, rax
    jnz .loop1

    mov rax, 1
    mov rdi,1 
    mov rdx,1
    .loop2: mov rsi,[rsp]
    lea rsi, [codes+rsi]
    syscall
    dec rbx
    pop rax
    mov rax,1
    test rbx,rbx
    jnz .loop2
    pop rbx
    ret




; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0
    jge .positive
    push rdi 
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .positive:
    call print_uint
    ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    ;check the length of both strings
    ;save the addr of str1
    push rdi
    call string_length
    mov rdi, rsi
    push rax

    push rsi
    call string_length
    pop rsi
    mov rdx, rax
    pop rax
    pop rdi
    cmp rax, rdx
    jne .not_equals
    mov rax, 0
    .loop:
        mov dl, [rdi+rax]
        cmp [rsi+rax], dl
        jne .not_equals
        cmp byte[rdi+rax], 0
        jz .end
        inc rax
        jmp .loop

    .not_equals:
        mov rax,0
        ret

    .end:
        mov rax,1                
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax,rax
    push rax
    mov rax, 0 
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    ;rdi rsi
    push rdi
    push rsi
    .check:
    call read_char
    cmp al, 0xA
    je .check
    pop rsi
    pop rdi
    mov rdx,0
    
    .loop:
    cmp rdx, rsi
    ja .bad_end
    cmp al, 0xA
    je .end
    cmp al, 0x0
    je .end
    mov byte[rdi+rdx], al
    inc rdx
    push rsi
    push rdi
    push rdx
    call read_char
    pop rdx
    pop rdi
    pop rsi
    jmp .loop
    .end:
    mov byte[rdi+rdx], 0
    call string_length
    mov rdx, rax
    mov rax, rdi
    ret
    .bad_end:
    mov rax,0
    mov rdx,0
    ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
    ;rdi rsi
    xor rdx, rdx
    xor rax, rax
    xor rcx, rcx
    .loop:
        mov cl, [rdi+rdx]
        cmp cl, 0
        je .nan
        cmp cl, '0'
        jb .nan
        cmp cl, '9'
        jg .nan
        inc rdx
        push rdx
        sub cl, '0'
        mov rsi, 10
        mul rsi
        add rax, rcx
        pop rdx
        jmp .loop
    .nan:
        ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    .check:
    cmp byte[rdi], '-'
    je .sign
    cmp byte[rdi], '+'
    je .sign
    cmp byte[rdi], '0'
    jb .end
    cmp byte[rdi], '9'
    ja .end
    call parse_uint
    ret
    .sign:
    inc rdi
    call parse_uint
    test rax,rax
    je .end
    cmp byte[rdi-1],'-'
    jne .continue
    push rax
    push rdx
    mov rdi, '-'
    call print_char
    pop rdx
    pop rax
    .continue:
    inc rdx
    .end:
    ret

    
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    ;rdi, rsi, rdx
    call string_length
    cmp rax, rdx
    jg .too_long

    xor rax, rax
    .check:
    cmp byte[rdi+rax], 0
    je .end
    mov cl, byte[rdi+rax]
    mov byte[rsi+rax], cl
    inc rax
    jmp .check

    .end:
    mov byte [rsi+rax], 0
    ret
    .too_long:
    mov rax,0
    ret
