%macro colon 2	
%ifid  %2
	%2: 
	%ifdef pointer_on_next
	dq pointer_on_next
	%else
	dq 0
	%endif
	%define pointer_on_next %2
%else
	%error "identificator was required"
%endif		
%ifstr %1
	db %1,0
%else
	%error "string was required"
%endif	  
		
%endmacro


