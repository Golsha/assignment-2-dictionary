section .text
global _start
%include "lib.inc"
;DON'T FORGET TO IMPORT THE FUNCTION STRING_LENGTH

find_word:
;rdi - pointer on the string (key)
;rsi - pointer on the beginning of the dict 
    .loop:
    cmp rsi, 0 
    je .fail
    push rsi 
    add rsi, 8 
    push rdi
    call string_equals 
    pop rdi
    pop rsi 
    cmp rax, 1 
    je .success
    mov rsi, [rsi] 
    jmp .loop
    
    .success:
    add rsi, 8
    mov rax, rsi 
    ret
    
    .fail:
    mov rax, 0 
    ret

