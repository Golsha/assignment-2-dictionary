%include "lib.inc"
%include "colon.inc"
%include "words.inc"
%include "dict.asm"
section .rodata
error_can_not_read: db "The word was too long for 255 digit buffer",0
error_not_found: db "Can't find this key in the dictionary",0

section .bss
buffer: resb 256

section .text
global _start

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор


print_error:
	call string_length
	mov rdx,rax
	mov rsi, rdi
	mov     rax, 1
    mov     rdi, 2
    syscall
	ret


section .text
_start:
mov rdi, buffer
mov rsi, 255
call read_word
cmp rax, 0
je .can_not_read

;mov rdi, rax
mov rdi, buffer
mov rsi, my_word
call find_word
cmp rax, 0 
je .not_found
;after calling the function i have the address of the beggining of key in rax
mov rdi, rax 
call string_length
add rdi, rax
inc rdi
call print_string
mov rdi, 0
call exit

.can_not_read:
mov rdi, error_can_not_read
jmp .error

.not_found:
mov rdi, error_not_found
jmp .error

.error:
call print_error
mov rdi, 1
call exit
